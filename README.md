# Python Week Of Code, Namibia 2019

Users not only want to create new entries but also edit existing entries.

## Task for Instructor

1. Add

   ```
   path('blog/<title>/edit/', views.blog_form, name='blog_edit')
   ```

   to [blog/urls.py](blog/urls.py).
1. Edit the function `blog_form` in [blog/forms.py](blog/forms.py) to

   ```
   def blog_form(request, title=None):
       if title:
           post = get_object_or_404(
               Post,
               title__iexact=title
           )
       else:
           post = None

       form = PostForm(
           request.POST if request.POST else None,
           instance=post
       )
       if request.POST:
           if form.is_valid():
               post = form.save()
               return redirect('blog', title=post.title)

       return render(
           request,
           'blog/post_form.html',
           {
               "form": form,
           }
       )
   ```
2. Add

   ```
   <a href="{% url 'blog_edit' post.title %}">Edit</a>
   ```

   to [blog/templates/blog/post.html](blog/templates/blog/post.html).

## Tasks for Learners

1. Add a URL to delete one blog post.
2. Add a view to delete one blog post.
3. Add a link to delete one blog post.